package com.costular.marvelheroes.domain.usecase

import com.costular.marvelheroes.domain.model.MarvelHero
import com.costular.marvelheroes.data.repository.MarvelHeroesRepositoryImpl
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by costular on 17/03/2018.
 */
class GetMarvelHeroesList @Inject constructor(val marvelHeroesRepositoryImpl: MarvelHeroesRepositoryImpl)
    : UseCase<List<MarvelHero>>() {

    override fun buildCase(): Observable<List<MarvelHero>> =
            marvelHeroesRepositoryImpl.getMarvelHeroesList()

}