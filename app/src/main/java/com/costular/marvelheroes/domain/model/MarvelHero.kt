package com.costular.marvelheroes.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by costular on 16/03/2018.
 */
@Parcelize
data class MarvelHero(
        val id: Int,
        val name: String = "",
        val photoUrl: String = "",
        val realName: String = "",
        val height: String = "",
        val power: String = "",
        val abilities: String = "",
        val groups: List<String> = emptyList<String>(),
        var favorite: Boolean
) : Parcelable