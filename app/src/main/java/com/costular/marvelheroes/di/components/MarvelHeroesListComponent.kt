package com.costular.marvelheroes.di.components

import com.costular.marvelheroes.di.modules.GetMarvelHeroesListModule
import com.costular.marvelheroes.di.scopes.PerActivity
import com.costular.marvelheroes.presentation.heroeslist.HeroesListActivity
import dagger.Component
import dagger.Subcomponent

/**
 * Created by costular on 17/03/2018.
 */
@PerActivity
@Subcomponent(modules = [GetMarvelHeroesListModule::class])
interface MarvelHeroesListComponent {
    fun inject(marvelListActivity: HeroesListActivity)
}