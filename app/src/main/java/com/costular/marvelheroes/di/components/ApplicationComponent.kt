package com.costular.marvelheroes.di.components

import com.costular.marvelheroes.di.modules.ApplicationModule
import com.costular.marvelheroes.di.modules.DataModule
import com.costular.marvelheroes.di.modules.NetModule
import com.costular.marvelheroes.presentation.heroedetail.HeroDetailActivity
import com.costular.marvelheroes.presentation.heroeslist.HeroesListActivity
import dagger.Component
import io.keepcoding.userlist.util.mvvm.ViewModelModule
import javax.inject.Singleton

/**
 * Created by costular on 16/03/2018.
 */
@Singleton
@Component(modules = [ApplicationModule::class, NetModule::class, DataModule::class, ViewModelModule::class])
interface ApplicationComponent {
    val marvelHeroesListComponent: MarvelHeroesListComponent
    fun inject(heroDetailActivity: HeroDetailActivity)
}