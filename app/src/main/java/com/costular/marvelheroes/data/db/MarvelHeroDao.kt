package com.costular.marvelheroes.data.db

import android.arch.persistence.room.*
import com.costular.marvelheroes.data.model.MarvelHeroEntity
import io.reactivex.Maybe


@Dao
abstract class MarvelHeroDao {
    @Query("SELECT * FROM marvelHeroes")
    abstract fun getAllMarvelHeroes(): Maybe<List<MarvelHeroEntity>>

    @Query("SELECT * FROM marvelHeroes WHERE id = :id")
    abstract fun getMarvelHero(id: Int): Maybe<MarvelHeroEntity>

    @Query("UPDATE marvelHeroes SET favorite = :favorite WHERE id = :id")
    abstract fun updateMarvelHero(id: Int, favorite: Boolean): Int

    @Update
    abstract fun updateMarvelHero(heroEntity: MarvelHeroEntity): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(marvelHeroes: List<MarvelHeroEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertHero(marvelHero: MarvelHeroEntity)

    @Query("DELETE FROM marvelHeroes")
    abstract fun deleteAllMarvelHeroes()

    @Transaction
    open fun removeAndInsertMarvelHeroes(marvelHeroes: List<MarvelHeroEntity>) {
        deleteAllMarvelHeroes()
        insertAll(marvelHeroes)
    }
}