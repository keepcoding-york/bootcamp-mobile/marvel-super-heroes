package com.costular.marvelheroes.data.model.mapper

import com.costular.marvelheroes.domain.model.MarvelHero
import com.costular.marvelheroes.data.model.MarvelHeroEntity

class MarvelHeroEntityMapper  : Mapper<MarvelHero, MarvelHeroEntity> {

    override fun transform(input: MarvelHero): MarvelHeroEntity =
            MarvelHeroEntity(
                    input.id,
                    input.name,
                    input.photoUrl,
                    input.realName,
                    input.height,
                    input.power,
                    input.abilities,
                    input.groups.toString(),
                    input.favorite)

    override fun transformList(inputList: List<MarvelHero>): List<MarvelHeroEntity> =
            inputList.map { transform(it) }
}