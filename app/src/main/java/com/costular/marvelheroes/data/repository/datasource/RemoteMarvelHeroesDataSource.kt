package com.costular.marvelheroes.data.repository.datasource

import com.costular.marvelheroes.data.net.MarvelHeroesService
import com.costular.marvelheroes.data.model.MarvelHeroEntity
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by costular on 17/03/2018.
 */
class RemoteMarvelHeroesDataSource(private val marvelHeroesService: MarvelHeroesService) :
        MarvelHeroesDataSource {

    override fun getMarvelHeroesList(): Observable<List<MarvelHeroEntity>> =
            marvelHeroesService.getMarvelHeroesList()

    fun updateMarvelHero(heroId: Int, isFavorite: Boolean): Single<MarvelHeroEntity> =
            marvelHeroesService.updateMarvelHero(heroId, isFavorite)

    fun getMarvelHero(heroId: Int): Single<MarvelHeroEntity> =
            marvelHeroesService.getMarvelHero(heroId)

}