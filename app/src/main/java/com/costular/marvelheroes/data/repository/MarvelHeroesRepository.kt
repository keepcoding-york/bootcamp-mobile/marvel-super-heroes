package com.costular.marvelheroes.data.repository

import com.costular.marvelheroes.domain.model.MarvelHero
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by costular on 17/03/2018.
 */
interface MarvelHeroesRepository {
    fun getMarvelHeroesList(): Observable<List<MarvelHero>>
    fun getMarvelHero(heroId: Int): Observable<MarvelHero>
    fun updateMarvelHero(heroId: Int, isFavorite: Boolean): Single<MarvelHero>
}