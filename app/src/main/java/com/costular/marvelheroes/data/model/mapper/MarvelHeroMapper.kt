package com.costular.marvelheroes.data.model.mapper

import com.costular.marvelheroes.domain.model.MarvelHero
import com.costular.marvelheroes.data.model.MarvelHeroEntity

/**
 * Created by costular on 17/03/2018.
 */
class MarvelHeroMapper : Mapper<MarvelHeroEntity, MarvelHero> {

    override fun transform(input: MarvelHeroEntity): MarvelHero =
            MarvelHero(
                    input.id,
                    input.name,
                    input.photoUrl,
                    input.realName,
                    input.height,
                    input.power,
                    input.abilities,
                    getGroupsFromMarvelHero(input),
                    input.favorite)

    override fun transformList(inputList: List<MarvelHeroEntity>): List<MarvelHero> =
            inputList.map { transform(it) }


    private fun getGroupsFromMarvelHero(marvelHeroEntity: MarvelHeroEntity): List<String> =
            marvelHeroEntity.groups.replace("\\s".toRegex(), "").split(",").toList()

}