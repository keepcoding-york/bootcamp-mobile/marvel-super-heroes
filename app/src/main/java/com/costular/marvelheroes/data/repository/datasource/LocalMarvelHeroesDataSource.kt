package com.costular.marvelheroes.data.repository.datasource

import com.costular.marvelheroes.data.db.MarvelHeroDatabase
import com.costular.marvelheroes.data.model.MarvelHeroEntity
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 * Created by costular on 05/06/2018.
 */
class LocalMarvelHeroesDataSource(val marvelHeroDatabase: MarvelHeroDatabase) : MarvelHeroesDataSource {

    override fun getMarvelHeroesList(): Observable<List<MarvelHeroEntity>> =
            marvelHeroDatabase.getMarvelHeroDao()
                    .getAllMarvelHeroes()
                    .toObservable()

    fun saveUsers(marvelHeroes: List<MarvelHeroEntity>) {
        Observable.fromCallable {
            marvelHeroDatabase.getMarvelHeroDao().removeAndInsertMarvelHeroes(marvelHeroes)
        }
                .subscribeOn(Schedulers.io())
                .subscribe()
    }

    fun getMarvelHero(heroId: Int): Single<MarvelHeroEntity> =
            marvelHeroDatabase.getMarvelHeroDao()
                    .getMarvelHero(heroId)
                    .toSingle()

    fun updateMarvelHero(hero: MarvelHeroEntity) {
        Single.fromCallable {
            marvelHeroDatabase.getMarvelHeroDao()
                    .updateMarvelHero(hero)
        }
                .subscribeOn(Schedulers.io())
                .subscribe()
    }

    fun updateMarvelHero(heroId: Int, isFavorite: Boolean) {
        Observable.fromCallable {
            marvelHeroDatabase.getMarvelHeroDao()
                    .updateMarvelHero(heroId, isFavorite)
        }
                .subscribeOn(Schedulers.io())
                .subscribe()

    }

    fun insertMarvelHero(marvelHero: MarvelHeroEntity) {
        Observable.fromCallable {
            marvelHeroDatabase.getMarvelHeroDao().insertHero(marvelHero)
        }
                .subscribeOn(Schedulers.io())
                .subscribe()
    }

}