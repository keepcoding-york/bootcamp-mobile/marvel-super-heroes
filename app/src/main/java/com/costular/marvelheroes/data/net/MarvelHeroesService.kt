package com.costular.marvelheroes.data.net

import com.costular.marvelheroes.data.model.MarvelHeroEntity
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by costular on 17/03/2018.
 */
interface MarvelHeroesService {

    @GET("heroes")
    fun getMarvelHeroesList(): Observable<List<MarvelHeroEntity>>

    @FormUrlEncoded
    @PUT("/heroes/{id}")
    fun updateMarvelHero(@Path("id") id: Int, @Field("favorite") favorite: Boolean): Single<MarvelHeroEntity>

    @GET("heroes/{id}")
    fun getMarvelHero(@Path("id") id: Int): Single<MarvelHeroEntity>

}