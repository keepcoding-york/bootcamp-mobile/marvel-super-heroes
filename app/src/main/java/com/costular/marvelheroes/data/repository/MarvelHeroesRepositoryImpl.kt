package com.costular.marvelheroes.data.repository

import android.util.Log
import com.costular.marvelheroes.BuildConfig
import com.costular.marvelheroes.domain.model.MarvelHero
import com.costular.marvelheroes.data.model.mapper.MarvelHeroEntityMapper
import com.costular.marvelheroes.data.model.mapper.MarvelHeroMapper
import com.costular.marvelheroes.data.repository.datasource.LocalMarvelHeroesDataSource
import com.costular.marvelheroes.data.repository.datasource.RemoteMarvelHeroesDataSource
import com.costular.marvelheroes.data.model.MarvelHeroEntity
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.Observable
import io.reactivex.Single
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.strategy.SocketInternetObservingStrategy
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.InternetObservingSettings
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.NonNull
import io.reactivex.functions.Consumer
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toSingle
import io.reactivex.schedulers.Schedulers.single


/**
 * Created by costular on 17/03/2018.
 */

class MarvelHeroesRepositoryImpl(private val remoteMarvelHeroesDataSource: RemoteMarvelHeroesDataSource,
                                 private val localMarvelHeroesDataSource: LocalMarvelHeroesDataSource,
                                 private val marvelHeroesMapper: MarvelHeroMapper,
                                 private val marvelHeroEntityMapper: MarvelHeroEntityMapper)
    : MarvelHeroesRepository {

    private val settings = InternetObservingSettings
            .host(BuildConfig.API_URL_PING)
            .strategy(SocketInternetObservingStrategy())
            .build()

    override fun getMarvelHeroesList(): Observable<List<MarvelHero>> =
            ReactiveNetwork.checkInternetConnectivity(settings)
                    .subscribeOn(Schedulers.io())
                    .flatMapObservable { isConnectedToAPI ->
                        if (isConnectedToAPI) {
                            getMarvelHeroesFromRemoteAndLocal()
                        } else {
                            Observable.concatArrayEager(
                                    getMarvelHeroesFromLocal(),
                                    observeInternetConnectivityForHeroes()
                            )
                        }
                    }
                    .map { marvelHeroesMapper.transformList(it) }

    override fun getMarvelHero(heroId: Int): Observable<MarvelHero> =
            ReactiveNetwork.checkInternetConnectivity(settings)
                    .subscribeOn(Schedulers.io())
                    .flatMapObservable { isConnectedToAPI ->
                        if (isConnectedToAPI) {
                            getMarvelHeroFromRemoteAndLocal(heroId)
                        } else {
                            Observable.concatArrayEager(
                                    getMarvelHeroFromLocal(heroId).toObservable(),
                                    observeInternetConnectivityForHero(heroId)
                            )
                        }
                    }
                    .map { marvelHeroesMapper.transform(it) }

    override fun updateMarvelHero(heroId: Int, isFavorite: Boolean): Single<MarvelHero> =
            remoteMarvelHeroesDataSource.updateMarvelHero(heroId, isFavorite)
                    .doOnSuccess {
                        localMarvelHeroesDataSource.updateMarvelHero(it.id, it.favorite)
                    }
                    .map { marvelHeroesMapper.transform(it) }


    private fun getMarvelHeroesFromRemote(): Observable<List<MarvelHeroEntity>> =
            remoteMarvelHeroesDataSource.getMarvelHeroesList()
                    .doOnNext { localMarvelHeroesDataSource.saveUsers(it) }

    private fun getMarvelHeroesFromLocal(): Observable<List<MarvelHeroEntity>> =
            localMarvelHeroesDataSource.getMarvelHeroesList()

    private fun getMarvelHeroesFromRemoteAndLocal(): Observable<List<MarvelHeroEntity>> =
            Observable.concat(
                    getMarvelHeroesFromLocal(),
                    getMarvelHeroesFromRemote()
            )

    private fun getMarvelHeroFromRemote(heroId: Int): Single<MarvelHeroEntity> =
            remoteMarvelHeroesDataSource.getMarvelHero(heroId)
                    .doOnSuccess { localMarvelHeroesDataSource.insertMarvelHero(it) }

    private fun getMarvelHeroFromLocal(heroId: Int): Single<MarvelHeroEntity> =
            localMarvelHeroesDataSource.getMarvelHero(heroId)

    private fun getMarvelHeroFromRemoteAndLocal(heroId: Int): Observable<MarvelHeroEntity> =
            Observable.concat(
                    // TODO: Why toObservable()? and not Single?
                    getMarvelHeroFromLocal(heroId).toObservable(),
                    getMarvelHeroFromRemote(heroId).toObservable()
            )

    private fun observeInternetConnectivityForHeroes(): Observable<List<MarvelHeroEntity>> =
            ReactiveNetwork.observeInternetConnectivity(settings)
                    .subscribeOn(Schedulers.io())
                    .filter { it }
                    .flatMap {
                        getMarvelHeroesFromRemote()
                    }

    private fun observeInternetConnectivityForHero(heroId: Int): Observable<MarvelHeroEntity> =
            ReactiveNetwork.observeInternetConnectivity(settings)
                    .subscribeOn(Schedulers.io())
                    .filter { it }
                    .flatMap {
                        getMarvelHeroFromRemote(heroId).toObservable()
                    }
}