package com.costular.marvelheroes.data.model

import android.annotation.SuppressLint
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by costular on 17/03/2018.
 */
@Entity(tableName = "marvelHeroes")
data class MarvelHeroEntity(
        @PrimaryKey(autoGenerate = false)
        val id: Int,
        val name: String,
        @SerializedName("photo")
        val photoUrl: String,
        val realName: String,
        val height: String,
        val power: String,
        val abilities: String,
        val groups: String,
        val favorite: Boolean
)