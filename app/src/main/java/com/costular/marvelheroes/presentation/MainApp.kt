package com.costular.marvelheroes.presentation

import android.app.Application
import com.costular.marvelheroes.di.components.ApplicationComponent
import com.costular.marvelheroes.di.components.DaggerApplicationComponent
import com.costular.marvelheroes.di.components.MarvelHeroesListComponent
import com.costular.marvelheroes.di.modules.ApplicationModule
import com.facebook.stetho.Stetho

class MainApp : Application() {

    private val marvelHeroesListComponent: MarvelHeroesListComponent by lazy {
       appComponent.marvelHeroesListComponent
    }

    private val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Stetho.initializeWithDefaults(this);
    }

    companion object {
        lateinit var instance: MainApp
            private set
    }

    fun marvelHeroesListComponent(): MarvelHeroesListComponent {
        return marvelHeroesListComponent
    }

    fun appComponent(): ApplicationComponent {
        return appComponent
    }
}