package com.costular.marvelheroes.presentation.heroeslist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.Toast
import com.costular.marvelheroes.R
import com.costular.marvelheroes.domain.model.MarvelHero
import com.costular.marvelheroes.presentation.MainApp
import com.costular.marvelheroes.presentation.util.Navigator
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class HeroesListActivity : AppCompatActivity() {

    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var heroesListViewModel: HeroesListViewModel

    private lateinit var adapter: HeroesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUp()
    }

    fun inject() {
        MainApp.instance.marvelHeroesListComponent().inject(this)
    }

    private fun setUp() {
        setUpRecycler()
        setUpViewModel()
    }

    private fun setUpRecycler() {
        adapter = HeroesListAdapter { heroId, image -> goToHeroDetail(heroId, image) }
        heroesListRecycler.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        heroesListRecycler.itemAnimator = DefaultItemAnimator()
        heroesListRecycler.adapter = adapter
    }

    private fun setUpViewModel() {
        heroesListViewModel = ViewModelProviders.of(this, viewModelFactory).get(HeroesListViewModel::class.java)
        bindEvents()
        heroesListViewModel.loadMarvelHeroes()
    }

    private fun bindEvents() {
        heroesListViewModel.isLoadingState.observe(this, Observer { isLoading ->
            isLoading?.let {
                showLoading(it)
            }
        })

        heroesListViewModel.heroesListState.observe(this, Observer { heroesList ->
            heroesList?.let {
                showHeroesList(it)
            }
        })

        heroesListViewModel.isErrorState.observe(this, Observer { isError ->
            isError?.let {
                showError(it)
            }
        })
    }

    private fun goToHeroDetail(heroId: Int, image: View) {
        navigator.goToHeroDetail(this, heroId, image)
    }

    private fun showLoading(isLoading: Boolean) {
        heroesListLoading.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun showHeroesList(heroes: List<MarvelHero>) {
        adapter.swapData(heroes)
    }

    private fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}
