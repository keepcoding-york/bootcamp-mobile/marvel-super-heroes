package com.costular.marvelheroes.presentation.heroedetail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.costular.marvelheroes.R
import com.costular.marvelheroes.domain.model.MarvelHero
import com.costular.marvelheroes.presentation.MainApp
import com.jakewharton.rxbinding2.view.RxView
import kotlinx.android.synthetic.main.activity_hero_detail.*
import javax.inject.Inject

/**
 * Created by costular on 18/03/2018.
 */
class HeroDetailActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var heroDetailViewModel: HeroDetailViewModel
    private var heroId: Int = -1

    companion object {
        const val PARAM_HERO_ID = "HERO_ID"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hero_detail)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }
        supportPostponeEnterTransition() // Wait for image load and then draw the animation
        heroId = intent.extras.getInt(PARAM_HERO_ID, -1)
        setUp()
    }

    private fun setUp() {
        setUpViewModel()
    }

    private fun setUpViewModel() {
        heroDetailViewModel = ViewModelProviders.of(this, viewModelFactory).get(HeroDetailViewModel::class.java)
        bindEvents()
        heroDetailViewModel.loadMarvelHero(heroId)
    }

    fun inject() {
        MainApp.instance.appComponent().inject(this)
    }

    private fun bindEvents() {
        RxView.clicks(favoriteButton).subscribe {
            heroDetailViewModel.toggleFavorite(heroId)
        }

        heroDetailViewModel.heroState.observe(this, Observer { hero ->
            hero?.let {
                fillHeroData(hero)
            }
        })

        heroDetailViewModel.isErrorState.observe(this, Observer { isError ->
            isError?.let {
                showError(it)
            }
        })

        heroDetailViewModel.showMessageState.observe(this, Observer { isError ->
            isError?.let {
                showMessage(it)
            }
        })
    }

    private fun showMessage(message: String) {
        val snackBar = Snackbar.make(heroDetailScrollView, message, Snackbar.LENGTH_SHORT)
        val snackBarView = snackBar.view
        snackBarView.setBackgroundColor(Color.RED)
        snackBar.show()
    }

    private fun fillHeroData(hero: MarvelHero) {
        Glide.with(this)
                .load(hero.photoUrl)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        supportStartPostponedEnterTransition()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        supportStartPostponedEnterTransition()
                        return false
                    }
                })
                .into(heroDetailImage)

        heroDetailName.text = hero.name
        heroDetailRealName.text = hero.realName
        heroDetailHeight.text = hero.height
        heroDetailPower.text = hero.power
        heroDetailAbilities.text = hero.abilities
        toggleFavorite(hero.favorite)
    }

    private fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun toggleFavorite(isFavorite: Boolean) {
        if (isFavorite) {
            favoriteButton.alpha = 1F
        } else {
            favoriteButton.alpha = 0.3F
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}