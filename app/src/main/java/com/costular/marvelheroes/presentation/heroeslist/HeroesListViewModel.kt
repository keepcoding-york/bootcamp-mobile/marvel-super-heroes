package com.costular.marvelheroes.presentation.heroeslist

import android.arch.lifecycle.MutableLiveData
import com.costular.marvelheroes.domain.model.MarvelHero
import com.costular.marvelheroes.domain.usecase.GetMarvelHeroesList
import io.keepcoding.userlist.util.mvvm.BaseViewModel
import javax.inject.Inject

class HeroesListViewModel @Inject constructor(private val getMarvelHeroesList: GetMarvelHeroesList) : BaseViewModel() {
    val isLoadingState: MutableLiveData<Boolean> = MutableLiveData()
    val isErrorState: MutableLiveData<String> = MutableLiveData()
    val heroesListState: MutableLiveData<List<MarvelHero>> = MutableLiveData()

    fun loadMarvelHeroes() {
        isLoadingState.postValue(true)
        getMarvelHeroesList.execute({ heroes ->
            heroesListState.value = heroes
            isLoadingState.postValue(false)
        }, {
            isErrorState.postValue(it.toString())
            isLoadingState.postValue(false)
        })
        compositeDisposable.add(getMarvelHeroesList.disposable)
    }
}