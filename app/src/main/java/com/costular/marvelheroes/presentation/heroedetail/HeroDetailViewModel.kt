package com.costular.marvelheroes.presentation.heroedetail

import android.arch.lifecycle.MutableLiveData
import com.costular.marvelheroes.domain.model.MarvelHero
import com.costular.marvelheroes.data.repository.MarvelHeroesRepositoryImpl
import io.keepcoding.userlist.util.mvvm.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class HeroDetailViewModel @Inject constructor(private val marvelHeroesRepositoryImpl: MarvelHeroesRepositoryImpl) : BaseViewModel() {
    val isErrorState: MutableLiveData<String> = MutableLiveData()
    val showMessageState: MutableLiveData<String> =  MutableLiveData()
    val heroState: MutableLiveData<MarvelHero> = MutableLiveData()

    fun loadMarvelHero(heroId: Int) {
        marvelHeroesRepositoryImpl.getMarvelHero(heroId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onError = {
                            isErrorState.postValue(it.toString())
                        },
                        onNext = {
                            heroState.value = it
                        }
                ).addTo(compositeDisposable)
    }

    fun toggleFavorite(heroId: Int) {
        val isFavorite = !heroState.value!!.favorite
        marvelHeroesRepositoryImpl.updateMarvelHero(heroId, isFavorite)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onError = {
                            isErrorState.postValue(it.toString())
                        },
                        onSuccess = {
                            heroState.value = it
                            showMessageState.value = "Saved favorite!"
                        }
                ).addTo(compositeDisposable)
    }
}